import React from 'react';
import renderer from 'react-test-renderer';

import Intro from './Intro';

describe('<Intro/>', () => {
	const tree = renderer.create(<Intro />).toJSON();
	it('has 2 children', () => {
		expect(tree.children.length).toBe(2);
	});
	it('renders correctly', () => {
		expect(tree).toMatchSnapshot();
	});
});
