# Plan for LookSmart

## Do as soon as possible

- Send necessary information from backend for display page
  - Handle all necessary backend information for the page view and pictures
- Create placeholder powerpoint images for each outfit
------------L-a-u-n-c-h------------------------
- Refactor code so development isn't a nightmare
- Format consistently for mobile and tablets
- Add Jest testing suite to app frontend and backend
- Add Card flip
- Send to Expo and test on iOS
  - Deploy to MacinCloud server to test with Xcode
- Provide 5+ semantic text options per temperature

---

- Setup Alkamyst Jira, Trello, Medium accounts
  - Create Dedicated board for LookSmart and add all feature ideas to it
  - Plan Roadmap for LookSmart
  - Create Contacts list for LookSmart for users and there categories
  - Integrate Optimizely with LookSmart for feature flagging and user grouping
- Serve static images via a CDN
- Implement via serverless backend (assuming low traffic)

---

## Features

- Ask if the user dresses more femininely or masculinely; store in AsyncStorage
- Implement User Accounts
- Pull time from system for notifications
  - About to rain
  - Weather when you first wake up
- Adjust Suggestions per temperature range per user (Manually via sliders)
  - Autonomously (via machine learning or another means)
