import Constants from 'expo-constants';
import React, { useState } from 'react';
import {
	StyleSheet,
	Text,
	View,
	AsyncStorage, // deprecated
} from 'react-native';
// import { AsyncStorage } from '@react-native-community/async-storage'; // Must eject app from expo to use
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';

import { Appearance } from 'react-native-appearance';

import LocationScreen from './LocationScreen';
// import UserHome from './UserHome';
import UserHomeLight from './UserHomeLight';
import UserHomeDark from './UserHomeDark';
import AuthHome from './AuthHome';
import AuthLogin from './AuthLogin';

export default function App(props) {
	const [isLoadingComplete, setLoadingComplete] = useState(false);
	const [userLocation, setUserLocation] = useState('');
	const [weatherData, setWeatherData] = useState();
	const [localLocation, setLocalLocation] = useState('');

	const [loginPress, setLoginPress] = useState(false);
	const [signupPress, setSignupPress] = useState(false);

	const fetchData = async (enteredLocation) => {
		const response = await fetch(
			'https://looksmart-backend.herokuapp.com/api/location/',
			{
				method: 'POST',
				headers: { 'Content-Type': 'application/json;charset=UTF-8' },
				body: JSON.stringify({ location: enteredLocation }),
				mode: 'cors',
				cache: 'default',
			}
		);

		const result = await response.json();
		setWeatherData(result.data);
	};

	warmUpServerlessBackend = async () => {
		const response = await fetch(
			'https://looksmart-backend.herokuapp.com/api/location/',
			{
				method: 'POST',
				headers: { 'Content-Type': 'application/json;charset=UTF-8' },
				body: JSON.stringify({ location: 'Nashville' }),
				mode: 'cors',
				cache: 'default',
			}
		);
	};

	warmUpServerlessBackend();

	const locationInputHandler = (enteredLocation) => {
		fetchData(enteredLocation);
		setUserLocation(enteredLocation);
		// Initiate loading screen
	};

	const loginHandler = () => {
		setLoginPress(true);
	};

	const signupHandler = () => {
		console.log('Not yet implemented');
	};

	const locationResetHandler = () => {
		setWeatherData();
	};

	let content = <LocationScreen onLocationEntered={locationInputHandler} />;

	// const version = convertBuildVersionToFloat(Constant);
	// if (version > 1){
	if (Constants.nativeAppVersion > 1) {
		if (loginPress) {
			content = <AuthLogin></AuthLogin>;
		} else {
			content = (
				<AuthHome
					onLoginClick={loginHandler}
					onSignupClick={signupHandler}
				></AuthHome>
			);
		}
	}

	if (weatherData) {
		let colorScheme = Appearance.getColorScheme();

		// let subscription = Appearance.addChangeListener(({ colorScheme }) => {
		if (colorScheme === 'dark') {
			content = (
				<UserHomeDark
					version={Constants.nativeAppVersion}
					savedLocation={userLocation}
					weather={weatherData}
					onLocationReset={locationResetHandler}
				/>
			);
		} else {
			content = (
				<UserHomeLight
					version={Constants.nativeAppVersion}
					savedLocation={userLocation}
					weather={weatherData}
					onLocationReset={locationResetHandler}
				/>
			);
		}
		// });

		// when you're done
		// subscription.remove();
	}

	async function loadResourcesAsync() {
		await Promise.all([
			Asset.loadAsync([
				require('./static/people/f-coat-hat.png'),
				require('./static/people/f-coat.png'),
				require('./static/people/f-dress.png'),
				require('./static/people/f-jacket.png'),
				require('./static/people/f-long-slv.png'),
				require('./static/people/f-short-slv.png'),
				require('./static/people/f-shorts.png'),
				require('./static/people/m-coat-hat.png'),
				require('./static/people/m-coat.png'),
				require('./static/people/m-jacket.png'),
				require('./static/people/m-long-slv.png'),
				require('./static/people/m-short-slv.png'),
				require('./static/people/m-shorts.png'),

				require('./static/weather-icons/clear-day.png'),
				require('./static/weather-icons/clear-night.png'),
				require('./static/weather-icons/cloudy.png'),
				require('./static/weather-icons/fog.png'),
				require('./static/weather-icons/partly-cloudy-day.png'),
				require('./static/weather-icons/partly-cloudy-night.png'),
				require('./static/weather-icons/rain-hard.png'),
				require('./static/weather-icons/rain-lightning.png'),
				require('./static/weather-icons/rain.png'),
				require('./static/weather-icons/snow.png'),
				require('./static/weather-icons/winter-mix.png'),
			]),
		]);
	}

	function sendErrorToMonitoringService(error) {
		console.warn(
			'Not Implemented Yet. But I hear \
           Sentry is good for this.'
		);
		console.warn(error);
	}

	function handleLoadingError(error) {
		sendErrorToMonitoringService(error);
	}

	function handleFinishLoading(setLoadingComplete) {
		setLoadingComplete(true);
	}

	if (!isLoadingComplete && !props.skipLoadingScreen) {
		return (
			<AppLoading
				startAsync={loadResourcesAsync}
				onError={handleLoadingError}
				onFinish={() => handleFinishLoading(setLoadingComplete)}
			/>
		);
	} else {
		return <View style={styles.container}>{content}</View>;
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
