import React, { useState } from 'react';
import {
    View,
    Text,
    TextInput,
    Button,
    ImageBackground,
    TouchableHighlight,
    Image,
} from 'react-native';

const AuthHome = props => {
    const [thing, setThing] = useState('');

    // const locationTextHandler = () => {
    //     // loadingScreen()
    //     // fetchData()
    //     props.onLocationEntered(enteredLocation);
    // };

    const loginPressHandler = () => {
        props.onLoginClick();
    }

    return (
        <ImageBackground
            source={require('./static/bg.png')}
            style={{ width: '100%', height: '100%' }}
        >
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%', height: '100%'
            }}>
                    <ImageBackground
                        source={require('./static/looksmart-logo.png')}
                        style={{ width: '90%', height: '41%', marginLeft: 40}}
                >
                    </ImageBackground>
                    <TouchableHighlight style={{ width: '55%', height: '15%' }} onPress={loginPressHandler}>
                        <Image source={require('./static/login.png')} style={{ width: '100%', height: '100%' }}></Image>
                    </TouchableHighlight>
                    <TouchableHighlight style={{ width: '55%', height: '15%' }} >
                        <Image source={require('./static/sign-up.png')} style={{ width: '100%', height: '100%' }}></Image>
                    </TouchableHighlight>
                {/* <TextInput style={{
                    // justifyContent:'center',
                    // alignContent:'center',
                    fontSize: 18,
                    height: 70,
                    minWidth: 250,
                    width: 350,
                    elevation: 7,
                    backgroundColor: '#FFFFFF',
                    borderColor: '#DDDDDD',
                    borderWidth: 1,
                    paddingHorizontal: 10,
                }}
                    placeholder="Location"
                    placeholderTextColor='gray'
                    onChangeText={inputText => setEnteredLocation(inputText)}
                    value={enteredLocation}
                    onSubmitEditing={locationTextHandler}
                ></TextInput> */}
            </View>
        </ImageBackground>
    );
}

export default AuthHome;