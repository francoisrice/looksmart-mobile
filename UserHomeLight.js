import React, { useState } from 'react';
import {
	View,
	Button,
	Text,
	TextInput,
	AsyncStorage,
	Image,
	ImageBackground,
	TouchableHighlight,
} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
// import { Linking } from 'expo';

getClothingImage = (temperatureKey) => {
	// Style_Clothes() by gender/preference
	genderStyle = Math.floor(Math.random() * 2);

	if (genderStyle) {
		clothing = {
			'100': require('./static/people/m-shorts.png'),
			'80': require('./static/people/m-short-slv.png'),
			'66': require('./static/people/m-long-slv.png'),
			'55': require('./static/people/m-jacket.png'),
			'40': require('./static/people/m-coat.png'),
			'30': require('./static/people/m-coat-hat.png'),
			'19': require('./static/people/m-coat-hat.png'),
		};
	} else {
		clothing = {
			'100': require('./static/people/f-shorts.png'),
			'80': require('./static/people/f-short-slv.png'),
			'66': require('./static/people/f-long-slv.png'),
			'55': require('./static/people/f-jacket.png'),
			'40': require('./static/people/f-coat.png'),
			'30': require('./static/people/f-coat-hat.png'),
			'19': require('./static/people/f-coat-hat.png'),
		};
	}
	let clothingAsset = clothing[temperatureKey];
	return clothingAsset;
};

getWeatherImage = (currentImage) => {
	let currentWeatherImages = {
		'misc': require('./static/weather-icons/rain-hard.png'),
		'clear-day': require('./static/weather-icons/clear-day.png'),
		'clear-night': require('./static/weather-icons/clear-night.png'),
		'cloudy': require('./static/weather-icons/cloudy.png'),
		'fog': require('./static/weather-icons/cloudy.png'),
		'partly-cloudy-day': require('./static/weather-icons/partly-cloudy-day.png'),
		'partly-cloudy-night': require('./static/weather-icons/partly-cloudy-night.png'),
		'rain': require('./static/weather-icons/rain.png'),
		'snow': require('./static/weather-icons/snow.png'),
		'sleet': require('./static/weather-icons/winter-mix.png'),
	};
	let currentWeatherImage = currentWeatherImages[currentImage];
	if (!currentWeatherImage) {
		currentWeatherImage = currentWeatherImages['misc'];
	}
	return currentWeatherImage;
};

const UserHomeLight = (props) => {
	const [currentLocation, setCurrentLocation] = useState(props.savedLocation);

	const locationTextHandler = () => {
		props.onLocationReset();
	};

	let suggestion = props.weather.recommendation;
	let mainImage = getClothingImage(props.weather.temperatureKey);
	let temp = Math.round(props.weather.temperature);
	let currentWeatherImage = getWeatherImage(props.weather.currentWeatherImage);
	let pollenImage = require('./static/alert_spacer.png');
	let rainImage = require('./static/alert_spacer.png');

	if (props.version > 1) {
		if (props.weather.pollen != [null, null]) {
			pollenImage = require('./static/alert_pollen.png');
			// Add something about pollen to the recommendation
		}
	} else {
		if (props.weather.pollen) {
			pollenImage = require('./static/alert_pollen.png');
			// Add blurb about bring an umbrella to recommendation
		}
	}

	if (props.weather.rain[0] != null || props.weather.rain[1] != null) {
		rainImage = require('./static/alert_umbrella.png');
	}

	forecastIcons = [];
	for (
		forecastNumber = 0;
		forecastNumber < props.weather.futureForecast.length;
		forecastNumber++
	) {
		forecastIcons[forecastNumber] = getWeatherImage(
			props.weather.futureForecast[forecastNumber].icon
		);
	}

	let forecastTime1 = props.weather.futureForecast[0].time;
	let forecastImage1 = forecastIcons[0];
	let forecastTemp1 = Math.round(props.weather.futureForecast[0].temp);

	let forecastTime2 = props.weather.futureForecast[1].time;
	let forecastImage2 = forecastIcons[1];
	let forecastTemp2 = Math.round(props.weather.futureForecast[1].temp);

	let forecastTime3 = props.weather.futureForecast[2].time;
	let forecastImage3 = forecastIcons[2];
	let forecastTemp3 = Math.round(props.weather.futureForecast[2].temp);

	let forecastTime4 = props.weather.futureForecast[3].time;
	let forecastImage4 = forecastIcons[3];
	let forecastTemp4 = Math.round(props.weather.futureForecast[3].temp);

	return (
		<ImageBackground
			source={require('./static/light/bg2.png')}
			style={{ width: '100%', height: '100%' }}
		>
			<View
				style={{
					flex: 1,
					paddingTop: 30,
				}}
			>
				<ImageBackground
					source={require('./static/light/card_main.png')}
					style={{ width: '100%', height: '99%' }}
				>
					<View
						style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
							paddingLeft: 0,
							paddingBottom: 0,
							padding: 20,
						}}
					>
						<TouchableHighlight
							style={{ width: '10%', height: '7%' }}
							onPress={locationTextHandler}
						>
							<Image
								source={require('./static/ribbon-tab.png')}
								style={{ width: '100%', height: '100%' }}
							></Image>
						</TouchableHighlight>
						<Image
							source={mainImage}
							style={{ width: '30%', height: '90%', resizeMode: 'contain' }}
						/>
						<View
							style={{
								justifyContent: 'space-between',
								height: '35%',
								paddingTop: 15,
							}}
						>
							<View
								style={{
									marginLeft: 90,
									flexDirection: 'row',
									justifyContent: 'flex-start',
									width: '22%',
									height: '43%',
								}}
							>
								<Image
									source={pollenImage}
									style={{
										width: '100%',
										height: '100%',
										resizeMode: 'contain',
									}}
								></Image>
								<Image
									source={rainImage}
									style={{
										width: '100%',
										height: '100%',
										resizeMode: 'contain',
									}}
								></Image>
							</View>
							<View style={{ flexDirection: 'row' }}>
								<Image
									source={currentWeatherImage}
									style={{ width: 100, height: 115, resizeMode: 'contain' }}
								></Image>
								<Text style={{ fontSize: 50, color: '#adadb1' }}>{temp}</Text>
								<View style={{ paddingTop: 15, width: '12%', height: '40%' }}>
									<Image
										source={require('./static/degrees-F.png')}
										style={{
											justifyContent: 'space-between',
											width: '100%',
											height: '100%',
											resizeMode: 'contain',
										}}
									></Image>
								</View>
							</View>
							<Text
								style={{
									width: 180,
									fontSize: 18,
									height: '100%',
									color: '#8e8e92',
								}}
							>
								{suggestion}
								{/* When highlighting articles of clothing, use '#599edb' */}
							</Text>
						</View>
					</View>
					<View
						style={{
							flexDirection: 'row',
							justifyContent: 'center',
							alignContent: 'center',
							paddingLeft: 30,
							width: '100%',
							height: '15%',
							borderWidth: 1,
							borderBottomColor: 'black',
						}}
					>
						<View>
							<Text style={{ color: '#8e8e92' }}>{forecastTime1}</Text>
							<Image
								source={forecastImage1}
								style={{
									width: 33,
									height: 38,
									resizeMode: 'contain',
								}}
							></Image>
							<View style={{ flexDirection: 'row' }}>
								<Text style={{ fontSize: 20, color: '#adadb1' }}>
									{forecastTemp1}
								</Text>
								<View style={{ paddingTop: 5, width: '17%', height: '45%' }}>
									<Image
										source={require('./static/degrees-F.png')}
										style={{
											width: '100%',
											height: '100%',
											resizeMode: 'contain',
										}}
									></Image>
								</View>
							</View>
						</View>
						<View>
							<Text style={{ color: '#8e8e92' }}>{forecastTime2}</Text>
							<Image
								source={forecastImage2}
								style={{
									width: 33,
									height: 38,
									resizeMode: 'contain',
								}}
							></Image>
							<View style={{ flexDirection: 'row' }}>
								<Text style={{ fontSize: 20, color: '#adadb1' }}>
									{forecastTemp2}
								</Text>
								<View style={{ paddingTop: 5, width: '17%', height: '45%' }}>
									<Image
										source={require('./static/degrees-F.png')}
										style={{
											width: '100%',
											height: '100%',
											resizeMode: 'contain',
										}}
									></Image>
								</View>
							</View>
						</View>
						<View>
							<Text style={{ color: '#8e8e92' }}>{forecastTime3}</Text>
							<Image
								source={forecastImage3}
								style={{
									width: 33,
									height: 38,
									resizeMode: 'contain',
								}}
							></Image>
							<View style={{ flexDirection: 'row' }}>
								<Text style={{ fontSize: 20, color: '#adadb1' }}>
									{forecastTemp3}
								</Text>
								<View style={{ paddingTop: 5, width: '17%', height: '45%' }}>
									<Image
										source={require('./static/degrees-F.png')}
										style={{
											width: '100%',
											height: '100%',
											resizeMode: 'contain',
										}}
									></Image>
								</View>
							</View>
						</View>
						<View>
							<Text style={{ color: '#8e8e92' }}>{forecastTime4}</Text>
							<Image
								source={forecastImage4}
								style={{
									width: 33,
									height: 38,
									resizeMode: 'contain',
								}}
							></Image>
							<View style={{ flexDirection: 'row' }}>
								<Text style={{ fontSize: 20, color: '#adadb1' }}>
									{forecastTemp4}
								</Text>
								<View style={{ paddingTop: 5, width: '17%', height: '45%' }}>
									<Image
										source={require('./static/degrees-F.png')}
										style={{
											width: '100%',
											height: '100%',
											resizeMode: 'contain',
										}}
									></Image>
								</View>
							</View>
						</View>
					</View>
					<View
						style={{
							alignItems: 'center',
							// "justifyContent": "flex-end",
							paddingTop: '18%',
						}}
					>
						<Hyperlink
							linkText={(url) =>
								url == 'https://darksky.net/poweredby/' ? 'DarkSky' : url
							}
						>
							<Text style={{ fontWeight: 'bold' }}>Powered By Dark Sky</Text>
						</Hyperlink>
					</View>
				</ImageBackground>
			</View>
		</ImageBackground>
	);
};

export default UserHomeLight;
